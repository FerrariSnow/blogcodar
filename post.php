<!-- Helpers -->
<?php
  include_once("helpers/url.php");
?>
<!-- Inclusão do cabeçalho -->
<?php
  include_once("templates/header.php");

if(isset($_GET['id'])) {

  $postID = $_GET['id'];
  $currentPost;

  foreach($posts as $post) {

    if($post['id'] == $postID) {
      $currentPost = $post;
    }
  }
}
?>
  <main id= "post-container">
    <div class="content-container">
      <h1 id="main-title"><?= $currentPost['title'] ?></h1>
      <p id="post-description"><?= $currentPost['description'] ?></p>
      <div class="img-container">
        <img src="<?= $BASE_URL ?>/img/<?= $currentPost['img'] ?>" alt="<?= $currentPost['title'] ?>">
      </div>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit quisquam dolor numquam provident. Ab illum minima quidem atque, ex nam unde aut, nesciunt optio voluptatibus, ad possimus quod quisquam porro.
      Error eaque dicta unde exercitationem rem quasi vero consequuntur aliquam magni commodi id ea dolor maxime perspiciatis beatae quod repudiandae at necessitatibus nulla, dolores hic eveniet? Maiores, quod earum. Non?
      Cumque ex numquam quod voluptas laborum dolorum illum error. Ad quas dignissimos sint! Asperiores, debitis omnis perferendis ab consequatur nam deleniti, laborum maiores nostrum expedita nihil rem tempore ullam qui?
      Tenetur non asperiores temporibus maiores ratione officiis voluptatem illum quibusdam quasi, sequi, omnis consequatur, fuga atque? Perspiciatis, illum. Repudiandae consectetur minus ipsa ullam molestias, quas non eius iusto soluta inventore.</p>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit quisquam dolor numquam provident. Ab illum minima quidem atque, ex nam unde aut, nesciunt optio voluptatibus, ad possimus quod quisquam porro.
      Error eaque dicta unde exercitationem rem quasi vero consequuntur aliquam magni commodi id ea dolor maxime perspiciatis beatae quod repudiandae at necessitatibus nulla, dolores hic eveniet? Maiores, quod earum. Non?
      Cumque ex numquam quod voluptas laborum dolorum illum error. Ad quas dignissimos sint! Asperiores, debitis omnis perferendis ab consequatur nam deleniti, laborum maiores nostrum expedita nihil rem tempore ullam qui?
      Tenetur non asperiores temporibus maiores ratione officiis voluptatem illum quibusdam quasi, sequi, omnis consequatur, fuga atque? Perspiciatis, illum. Repudiandae consectetur minus ipsa ullam molestias, quas non eius iusto soluta inventore.</p>
    </div>
    <aside id="nav-container">
  <h3 id="tags-title">Tags</h3>
  <ul id="tag-list">
    <?php foreach($currentPost['tags'] as $tag): ?>
      <li><a href="#"><?= $tag ?></a></li>
    <?php endforeach; ?>  
  </ul>
  <h3 id="categories-title">Categorias</h3>
  <ul id="categories-list">
    <?php foreach($categories as $category): ?>
      <li><a href="#"><?= $category ?></a></li>
    <?php endforeach; ?>  
  </ul>
 </aside>
  </main>
 
<!-- Inclusão do rodapé -->
<?php
  include_once("templates/footer.php");
?>